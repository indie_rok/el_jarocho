<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">

        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>



    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


      <section class="header_top row">
        <section class="container">
          <section class="col-md-5  hidden-xs text-center">
            TOSTADOR, MOLINO Y EXPENDIO DE CAFÉ
          </section>

          <section class="col-md-2 text-center">
              <a href="https://www.facebook.com/cafeljarocho"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></a>
              <a href="https://twitter.com/CAFELJAROCHO"><i class="fa fa-twitter fa-lg" aria-hidden="true"></i></a>
              <a href="https://www.instagram.com/eljarochocafe/"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></a>
          </section>
          <section class="col-md-5  hidden-xs text-center">
            CAFE EL JAROCHO // COYOACÁN CIUDAD DE MÉXICO
          </section>
        </section>
      </section>


        <section class="row hidden-xs" style="margin-right:0px">
          <section class="col-md-12 imagen_header text-center top100" >
            <a href="index.php"><img src="images/logo_header.png" alt="" ></a>
            <a href="comprar.php" class=" top15 hidden-xs" style="position:absolute;right:30px;" ><img src="images/carro.png" alt=""><p>Pedidos</p></a>
          </section>
        </section>

        <nav class="navbar navbar-default main-menu " id="main-menu" role="navigation">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="comprar.php" class=" top15 visible-xs" style="position:absolute;right:30px;" ><img src="images/carro.png" alt=""><p class=" hidden-xs">Pedidos</p></a>

            <section class="visible-xs home_icon"> <a href="index.php"><img src="images/logo_mb.jpg" class="img-responsive"></a>
        </section>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse navbar-ex1-collapse">
              <ul class="nav navbar-nav">
                  <li><a href="index.php">Inicio</a></li>
                  <li><a href="historia.php" id="historia_menu">Historia</a></li>
                  <li><a href="menu.php" id="productos_menu">Productos</a></li>
                  <li><a href="sucursales.php" id="sucursales_menu">Sucursales</a></li>
                  <li><a href="comprar.php" id="contacto_menu">Contacto</a></li>
              </ul>
          </div><!-- /.navbar-collapse -->
        </nav>
