<?php require 'header.php' ?>


<div class="widewrapper main">
      <img src="images/slide_historia.jpg" class="img-responsive" alt="JOHN C. LOWERY, INC">
</div>


<section class="slide_home historia_jarocho">
  <section class="titulo_chido text-center" >
    <h2>Nuestra historia</h2>
    <h4>tostados, molino y expendio de café</h4>
    <img src="images/titulo_bar.png" class="center-block" alt="">

  </section>

  <section class="row top30">
    <section class="col-md-6 nopadding">
      <img src="images/historia_jarocho1.jpg"  class="img-responsive" alt="">
    </section>

    <section class="col-md-6 square_center">
      <p>Nuestra historia se remonta 63 años atrás… Gil Romero y Bertha Paredes (pareja fundadora) trabajaban en una tienda ubicada en la calle de Aguayo, en Coyoacán donde se vendían semillas y frutas traídas de Veracruz. Posteriormente Bertha Paredes queda como dueña de ese negocio que mas tarde se cambiaría de lugar a las calles de Cuauhtémoc y Allende en también en Coyoacán México, dando lugar a Café El Jarocho.</p>
      <p>En Café El Jarocho se elaboraba café de olla, café negro y café con leche de forma totalmente artesanal, ya que no se disponía de cafeteras. Gracias a la demanda de café por parte de los vecinos se adquirió una cafetera industrial con la cual comenzó la venta de bebidas mas especializadas como Café Express, Capuchino y Americano.</p>
    </section>
  </section>


  <section class="row">

    <section class="col-md-6 square_center">

      <p>Actualmente Café El Jarocho es reconocido en todo México como “El Aroma De Coyoacán” mismo que tomó presencia gracias al cariño de la gente que todos los días gusta de disfrutar un buen café. Por otra parte, Coyoacán como lugar cultural y turístico de la Ciudad de México da lugar a miles de personas que todas las semanas nos visitan atraídas por el aroma que despide nuestro café.</p>
      <p>Una de nuestras principales preocupaciones es siempre darle a la gente el café que se merece, por un lado teniendo una gran calidad y sabor, y por otro lado un precio justo.</p>
      <p>A la fecha contamos con 10 sucursales ubicadas principalmente en el centro de Coyoacán y alrededores como lo son en Miguel A. Quevedo, División del Norte, Taxqueña, extendiéndose hasta una sucursal en el Centro Histórico de la Ciudad de México.</p>
    </section>

    <section class="col-md-6 nopadding ">
      <img src="images/historia_jarocho2.jpg"  class="img-responsive" alt="">
    </section>

  </section>


    <section class="row">

      <section class="col-md-10 col-md-push-1 padding_fix2">
      <p>CAFÉ PREMIADO Y RECONOCIDO MUNDIALMENTE Gracias a ustedes, Café el Jarocho ha sido reconocido con grandes premios a nivel internacional como el Bizz Award 2006 al Business Excellence otorgado en Nueva York, como la Estrella de Oro al Mérito Empresarial por la Worldwide Marketing Organization en Holanda.</p>

      <p>NUESTRA PROMESA PARA LOS PRÓXIMOS AÑOS ES QUE SIGAS DISFRUTANDO DE UN GRAN CAFÉ, CON UN GRAN SABOR Y PRECIO JUSTO, ADEMÁS DE EXCELENTE CALIDAD, PERO SOBRE TODO ÚNICO </p>
    </section>

      <section class="col-md-6">
        <img src="images/premio1.jpg" alt="" class="img-responsive">
      </section>

      <section class="col-md-6">
        <img src="images/premio2.jpg" alt="" class="img-responsive">
      </section>

    </section>

</section>


<?php require 'footer.php' ?>
