<?php require 'header.php' ?>




<div class="widewrapper main">
      <img src="images/slide_sucursales.jpg" class="img-responsive" alt="JOHN C. LOWERY, INC">
</div>


<section class="slide_home sucursales">
  <section class="titulo_chido text-center" >
    <h2>Sucursales</h2>
    <h4>estamos cerca de ti</h4>
    <img src="images/titulo_bar.png" class="center-block" alt="">
<br><br>
  </section>

  <?php include 'functions.php';

      echo sucursal_row(
                            'sucursal_cuauhtemoc134',
                            'CUAHUTÉMOC ESQ. ALLENDE',
                            'CUAUHTÉMOC NO. 134 E, F Y G ESQ. ALLENDE
                            COL. DEL CARMEN COYOACÁN, CIUDAD DE MÉXICO
                            <br>5658 5029 Y 5554 5418',
                            //'http://bit.ly/2cgJDNg',
                            'https://www.google.com.mx/maps/dir/Cuauht%C3%A9moc+134,+Coyoac%C3%A1n,+04000+Ciudad+de+M%C3%A9xico,+D.F./@19.3510732,-99.1641188,17z/data=!4m16!1m7!3m6!1s0x85d1ffc49ed99193:0x71e0d31d4112e7ce!2sCuauht%C3%A9moc+134,+Coyoac%C3%A1n,+04000+Ciudad+de+M%C3%A9xico,+D.F.!3b1!8m2!3d19.3510682!4d-99.1619301!4m7!1m0!1m5!1m1!1s0x85d1ffc49ed99193:0x71e0d31d4112e7ce!2m2!1d-99.1619301!2d19.3510682',

                            'sucursal_centenario91',
                            'CENTENARIO 91',
                            'CENTENARIO NO. 91-B
                            COL. DEL CARMEN COYOACÁN, CIUDAD DE MÉXICO
                            <br>5658 5029 Y 5554 5418',
                            'https://www.google.com.mx/maps/dir/Centenario+91,+Del+Carmen,+04100+Ciudad+de+M%C3%A9xico,+D.F./@19.3520595,-99.1657804,17z/data=!4m16!1m7!3m6!1s0x85d1ffc374b518ed:0x50c3473b88f6b904!2sCentenario+91,+Del+Carmen,+04100+Ciudad+de+M%C3%A9xico,+D.F.!3b1!8m2!3d19.3520545!4d-99.1635917!4m7!1m0!1m5!1m1!1s0x85d1ffc374b518ed:0x50c3473b88f6b904!2m2!1d-99.1635917!2d19.3520545'

    );

    echo sucursal_row(
                          'sucursal_mexico25',
                          'AV.MÉXICO 25',
                          'AVENIDA MÉXICO NO. 25-C
                          COL. DEL CARMEN COYOACÁN, CIUDAD DE MÉXICO
                          <br>5658 5029 Y 5554 5418',
                          'https://www.google.com.mx/maps/dir/Av+M%C3%A9xico+25,+Del+Carmen,+04100+Ciudad+de+M%C3%A9xico,+D.F./@19.3521879,-99.1673766,17z/data=!4m16!1m7!3m6!1s0x85d1ffc2560171d7:0x2e72b056774ebd7b!2sAv+M%C3%A9xico+25,+Del+Carmen,+04100+Ciudad+de+M%C3%A9xico,+D.F.!3b1!8m2!3d19.3521829!4d-99.1651879!4m7!1m0!1m5!1m1!1s0x85d1ffc2560171d7:0x2e72b056774ebd7b!2m2!1d-99.1651879!2d19.3521829',

                          'sucursal_division2761',
                          'DIVISIÓN DEL NORTE 2761',
                          'DIVISIÓN DEL NORTE NO. 2761
                          COL. BARRIO DE SAN LUCAS, <br>CIUDAD DE MÉXICO
                          <br> 5658 5029 Y 5554 5418',
                          'https://www.google.com.mx/maps/dir/Av+Divisi%C3%B3n+del+Nte+2761,+San+Lucas,+04030+Ciudad+de+M%C3%A9xico,+D.F./@19.3498062,-99.1532288,17z/data=!4m16!1m7!3m6!1s0x85d1ffd201b0b121:0x1598fd37020bdce5!2sAv+Divisi%C3%B3n+del+Nte+2761,+San+Lucas,+04030+Ciudad+de+M%C3%A9xico,+D.F.!3b1!8m2!3d19.3498012!4d-99.1510401!4m7!1m0!1m5!1m1!1s0x85d1ffd201b0b121:0x1598fd37020bdce5!2m2!1d-99.1510401!2d19.3498012'

  );


  echo sucursal_row(
                        'sucursal_quevedo755',
                        'MIGUEL A. DE QUEVEDO 755',
                        'AV. MIGUEL ÁNGEL DE QUEVEDO NO. 755-B
                        COL. BARRIO NIÑO JESÚS, CIUDAD DE MÉXICO
                        <br>5658 5029 Y 5554 5418',
                        'https://www.google.com.mx/maps/place/Av+Miguel+%C3%81ngel+de+Quevedo+755,+San+Francisco,+04320+Ciudad+de+M%C3%A9xico,+D.F./@19.3439804,-99.162911,17z/data=!3m1!4b1!4m5!3m4!1s0x85d1ffdbddd05c83:0xd51758f622aa37cc!8m2!3d19.3439754!4d-99.1607223',

                        'sucursal_quevedo109',
                        'MIGUEL A. DE QUEVEDO 109',
                        'AV. MIGUEL ÁNGEL DE QUEVEDO NO. 109
                        COL. CHIMALISTAC, CIUDAD DE MÉXICO
                        <br>5658 5029 Y 5554 5418',
                        'https://www.google.com.mx/maps/dir/Avenida+Miguel+%C3%81ngel+de+Quevedo,+La+Concepci%C3%B3n,+04020+Ciudad+de+M%C3%A9xico,+D.F./@19.3439499,-99.1597383,17z/data=!4m21!1m7!3m6!1s0x85d1fffa6d0b7e11:0x3f2c0cc64777a3f1!2sAv+Miguel+%C3%81ngel+de+Quevedo+109,+Chimalistac,+%C3%81lvaro+Obreg%C3%B3n,+D.F.!3b1!8m2!3d19.3439449!4d-99.1575496!4m12!1m5!1m1!1s0x85d1ffdc4f426093:0x2ce8c5e32f3e57de!2m2!1d-99.1640909!2d19.3445523!1m5!1m1!1s0x85d1ffda027d51ad:0xb25510d6bdf5c5ca!2m2!1d-99.1575496!2d19.3439449'

);

echo sucursal_row(
                      'sucursal_quevedo560',
                      'MIGUEL A. DE QUEVEDO 560',
                      'AV. MIGUEL ÁNGEL DE QUEVEDO NO. 560
                      COL. SANTA CATARINA, CIUDAD DE MÉXICO
                      <br>5658 5029 Y 5554 5418',
                      'https://www.google.com.mx/maps/dir/Av+Miguel+%C3%81ngel+de+Quevedo+560,+Santa+Catarina,+04010+Ciudad+de+M%C3%A9xico,+D.F./@19.3453303,-99.1697802,17z/data=!4m16!1m7!3m6!1s0x85d1ffdd89aacc2d:0x20f290b6ff1e817a!2sAv+Miguel+%C3%81ngel+de+Quevedo+560,+Santa+Catarina,+04010+Ciudad+de+M%C3%A9xico,+D.F.!3b1!8m2!3d19.3453253!4d-99.1675915!4m7!1m0!1m5!1m1!1s0x85d1ffdd89aacc2d:0x20f290b6ff1e817a!2m2!1d-99.1675915!2d19.3453253',

                      'sucursal_taxquena1535',
                      'TAXQUEÑA 1535',
                      'AV. TAXQUEÑA NO. 1535
                      COL. PETROLERA TAXQUEÑA, CIUDAD DE MÉXICO
                      <br>5658 5029 Y 5554 5418',
                      'https://www.google.com.mx/maps/dir/Calz+Taxque%C3%B1a+1535,+Petrolera+Taxque%C3%B1a,+04200+Ciudad+de+M%C3%A9xico,+D.F./@19.3400882,-99.1347637,17z/data=!4m16!1m7!3m6!1s0x85d1fe2790f72305:0xe44b4cd8c4678069!2sCalz+Taxque%C3%B1a+1535,+Petrolera+Taxque%C3%B1a,+04200+Ciudad+de+M%C3%A9xico,+D.F.!3b1!8m2!3d19.3400832!4d-99.132575!4m7!1m0!1m5!1m1!1s0x85d1fe2790f72305:0xe44b4cd8c4678069!2m2!1d-99.132575!2d19.3400832'

);
  ?>


</section>

<?php require 'footer.php' ?>
