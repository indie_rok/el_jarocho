<?php

function sucursal_row($imagen,$titulo,$direccion,$url1,$imagen2,$titulo2,$direccion2,$url2){
  return <<<HTML
  <section class="row sucursal text-center ">
    <section class="col-md-6 nopadding ">
      <img src="images/sucursales/$imagen.jpg" class="img-responsive" alt="">
    </section>

    <section class="col-md-4 nopadding margin-fix col-md-push-1 padding_fix">
      <img src="images/marker-verde.png" alt="" class="top30 marker-verde">
      <h4><b>$titulo</b></h4>
      <h4>$direccion</h4>

      <a target="_blank"   href="$url1" class="btn btn-primary">Como llegar</a>
    </section>
  </section><!--/sucursal -->

  <section class="row sucursal text-center">
    <section class="col-md-6 nopadding padding_fix margin-fix  ">

      <img src="images/sucursales/$imagen2.jpg" class="img-responsive visible-xs" alt="">
      <img src="images/marker-verde.png" class="top50 marker-verde" alt="">
      <h4><b>$titulo2</b></h4>
      <h4>$direccion2</h4>

      <a target="_blank"   href="$url2" class="btn btn-primary">Como llegar</a>
    </section>

    <section class="col-md-6 nopadding padding_fix ">
      <img src="images/sucursales/$imagen2.jpg" class="img-responsive hidden-xs" alt="">
    </section>
  </section>
HTML;
}
