
// Contact Form Scripts

$("#contact-form").submit(function(e){
            e.preventDefault();
            var name = $("#form_name").val();
            var email = $("#form_email").val();
            var message = $("#form_message").val();

            $.ajax({
                  url: "./mail/contacto.php",
                  type: "POST",
                  data: {
                      name: name,
                      email: email,
                      message: message
                  },
                  cache: false,
                  success: function() {
                      // Success message
                      $('#success').html("<div class='alert alert-success text-center'>");
                      $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                          .append("</button>");
                      $('#success > .alert-success')
                          .append("<strong>Gracias por tu mensaje </strong>");
                      $('#success > .alert-success')
                          .append('</div>');

                      //clear all fields
                      $('#contact-form').trigger("reset");
                  },
                  error: function() {
                      // Fail message
                      $('#success').html("<div class='alert alert-danger'>");
                      $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                          .append("</button>");
                      $('#success > .alert-danger').append("<strong>No funciona el servidor, intenta después!");
                      $('#success > .alert-danger').append('</div>');
                      //clear all fields
                      $('#contactForm').trigger("reset");
                  },
              })

});
