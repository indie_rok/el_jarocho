$(document).ready(function() {

  $(window).scroll(function () {
      //if you hard code, then use console
      //.log to determine when you want the
      //nav bar to stick.
    if ($(window).scrollTop() > 240) {
      $('#main-menu').addClass('navbar-fixed');
      $('#main-menu').addClass('navbar-fixed-top');
    }
    if ($(window).scrollTop() < 241) {
      $('#main-menu').removeClass('navbar-fixed');
      $('#main-menu').removeClass('navbar-fixed-top');
    }
  });
});


$("#historia_menu").click(function() {
    $('html, body').animate({
        scrollTop: $("#nuestra_historia").offset().top - 100
    }, 1000);
});


$("#productos_menu").click(function() {
    $('html, body').animate({
        scrollTop: $("#nuestros_productos").offset().top - 100
    }, 1000);
});



$("#sucursales_menu").click(function() {
    $('html, body').animate({
        scrollTop: $("#nuestras_sucursales").offset().top - 100
    }, 1000);
});



$("#contacto_menu").click(function() {
    $('html, body').animate({
        scrollTop: $("#nuestro_contacto").offset().top - 100
    }, 1000);
});
