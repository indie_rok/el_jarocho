
// Contact Form Scripts


function submitForm(){

            var name = $('#form_name').val();
            var email_address = $('#form_email').val();
            var phone = $('#form_phone').val();
            var pais = $('#form_pais').val();
            var estado = $('#form_estado').val();
            var calle = $('#form_calle').val();
            var entre_calle = $('#form_entre_calle').val();
            var colonia = $('#form_colonia').val();
            var cp  = $('#form_cp').val();
            var tipo_cafe = $('#form_tipo_cafe').val();
            var tipo_molido = $('#form_tipo_molido').val();
            var cantidad_cafe = $('#form_cantidad_cafe').val();
            var message = $('#form_message').val();

            $.ajax({
                  url: "./mail/contacto_compra.php",
                  type: "POST",
                  data: {

                      name : name,
                      email_address : email_address,
                      phone : phone,
                      pais : pais,
                      estado : estado,
                      calle : calle,
                      entre_calle : entre_calle,
                      colonia : colonia,
                      cp  : cp,
                      tipo_cafe : tipo_cafe,
                      tipo_molido : tipo_molido,
                      cantidad_cafe : cantidad_cafe,
                      message : message
                  },
                  cache: false,
                  success: function() {
                      // Success message
                      $('#success').html("<div class='alert alert-success text-center'>");
                      $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                          .append("</button>");
                      $('#success > .alert-success')
                          .append("<strong>Tu pedido ha sido enviado, nos pondremos en contacto contigo. </strong>");
                      $('#success > .alert-success')
                          .append('</div>');

                      //clear all fields
                      $('#contact-form').trigger("reset");
                  },
                  error: function() {
                      // Fail message
                      $('#success').html("<div class='alert alert-danger'>");
                      $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                          .append("</button>");
                      $('#success > .alert-danger').append("<strong>No funciona el servidor, intenta después!");
                      $('#success > .alert-danger').append('</div>');
                      //clear all fields
                      $('#contactForm').trigger("reset");
                  },
              })


}
