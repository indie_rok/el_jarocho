<?php require 'header.php' ?>

<div class="widewrapper main text-center">
      <img src="images/slide_menu.jpg" class="img-responsive" alt="JOHN C. LOWERY, INC">
</div>

<section class="hidden-xs">
  <section class="row slide_home">
    <section class="col-md-12">
      <img src="images/titulo_menu_cafe.png" class="center-block img-responsive" alt="">
      <img src="images/titulo_bar.png" class="center-block top15" alt="">
    </section>
  </section>

  <section class="row ">
    <section class="col-md-4 nopadding">
      <img src="images/menu_cafe1.png" alt="" class="img-responsive center-block">
    </section>

    <section class="col-md-4 nopadding">
      <img src="images/menu_cafe1.jpg" class="img-responsive center-block" alt="">
    </section>

    <section class="col-md-4 nopadding">
      <img src="images/menu_cafe2.png"  class="img-responsive center-block" alt="">
    </section>

  </section>

  <section class="row ">
    <section class="col-md-4 nopadding">
      <img src="images/menu_cafe2.jpg" alt="" class="img-responsive center-block">
    </section>

    <section class="col-md-4 nopadding">
      <img src="images/menu_cafe3.png" class="img-responsive center-block" alt="">
    </section>

    <section class="col-md-4 nopadding">
      <img src="images/menu_cafe3.jpg"  class="img-responsive center-block" alt="">
    </section>

  </section>

<hr>

  <section class="row slide_home">
    <section class="col-md-12">
      <img src="images/titulo_menu_te.png" class="center-block img-responsive" alt="">
      <img src="images/titulo_bar.png" class="center-block top15" alt="">
    </section>
  </section>

  <section class="row slide_home">
    <section class="col-md-4">
      <img src="images/menu_te1.png" alt="" class="img-responsive center-block">
    </section>

    <section class="col-md-4">
      <img src="images/menu_te1.jpg" class="img-responsive center-block" alt="">
    </section>

    <section class="col-md-4">
      <img src="images/menu_te2.png"  class="img-responsive center-block" alt="">
    </section>
  </section>

<hr>

  <section class="row slide_home">
    <section class="col-md-12">
      <img src="images/titulo_menu_pan.png" class="center-block img-responsive" alt="">
      <img src="images/titulo_bar.png" class="center-block top15" alt="">
    </section>
  </section>

  <section class="row slide_home">
    <section class="col-md-4">
      <img src="images/menu_pan1.png" alt="" class="img-responsive center-block">
    </section>

    <section class="col-md-4">
      <img src="images/menu_pan1.jpg" class="img-responsive center-block" alt="">
    </section>

    <section class="col-md-4">
      <img src="images/menu_pan2.png"  class="img-responsive center-block" alt="">
    </section>
  </section>
  </section>

  <section class="row slide_home visible-xs">
    <section >

      <img src="images/titulo_menu_cafe.png" class="center-block img-responsive" style="width:220px" alt="">
      <img src="images/titulo_bar.png" class="center-block top15" alt="">

      <img src="images/menu_cafe1_mb.jpg" class="img-responsive center-block top30" alt="">
      <img src="images/menu_cafe2_mb.jpg" class="img-responsive center-block" alt="">
      <img src="images/menu_cafe3_mb.jpg" class="img-responsive center-block" alt="">

      <img src="images/titulo_menu_te.png" class="center-block img-responsive top30" style="width:220px" alt="">
      <img src="images/titulo_bar.png" class="center-block top15" alt="">

      <img src="images/menu_te1_mb.jpg" class="img-responsive center-block top30" alt="">
      <img src="images/menu_te2_mb.jpg" class="img-responsive center-block" alt="">


      <img src="images/titulo_menu_pan.png" class="center-block img-responsive top30" style="width:220px" alt="">
      <img src="images/titulo_bar.png" class="center-block top15" alt="">


      <img src="images/menu_pan1_mb.jpg" class="img-responsive center-block top30" alt="">
      <img src="images/menu_pan2_mb.jpg" class="img-responsive center-block" alt="">

    </section>
  </section>


<?php require 'footer.php' ?>
