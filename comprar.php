<?php require 'header.php' ?>

<div class="widewrapper main">
      <img src="images/slide_comprar.jpg" class="img-responsive" alt="JOHN C. LOWERY, INC">
</div>

<section class="titulo_chido text-center top15" >
  <h2>Disfruta nuestro café</h2>
  <h4>hasta la puerta de tu casa</h4>
  <img src="images/titulo_bar.png" class="center-block" alt="">
</section>

<section class="row top15 ">

  <section class="col-md-2 nopadding">
    <img src="images/cafe_calidad.jpg" class="img-responsive" alt="">
  </section>


  <section class="col-md-2  nopadding hidden-xs">
    <img src="images/cafe_casa.jpg" class="img-responsive" alt="">
  </section>

  <section class="col-md-2 nopadding hidden-xs">
    <img src="images/cafe_cubano.jpg" class="img-responsive" alt="">
  </section>

  <section class="col-md-2 nopadding hidden-xs">
    <img src="images/cafe_marago.jpg" class="img-responsive" alt="">
  </section>

  <section class="col-md-2 nopadding hidden-xs">
    <img src="images/cafe_descafeinado.jpg" class="img-responsive" alt="">
  </section>

  <section class="col-md-2 nopadding  hidden-xs">
    <img src="images/cafe_molido.jpg" class="img-responsive" alt="">
  </section>

</section>

<section class="row slide_home">
  <section class="titulo_chido text-center top15" >
    <h2>Haz tu pedido aquí</h2>
    <img src="images/titulo_bar.png" class="center-block" alt="">
  </section>

  <form id="contact-form-comprar" class="top30 contact-form " method="post" action="contact_form_compra.php" role="form">
  <div class="messages"></div>

      <div class="controls">
          <div class="row">
              <div class="col-md-6 col-md-push-3">
                  <div class="form-group">
                      <input id="form_name" type="text" name="name" class="form-control" placeholder="NOMBRE" required="required" data-error="Firstname is required.">
                      <div class="help-block with-errors"></div>
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-md-6 col-md-push-3">
                  <div class="form-group">
                      <input id="form_email" type="email" name="email" class="form-control" placeholder="EMAIL" required="required" data-error="Email is required.">
                      <div class="help-block with-errors"></div>
                  </div>
              </div>
          </div>

          <div class="row">
              <div class="col-md-6 col-md-push-3">
                  <div class="form-group">
                      <input id="form_phone" type="number" name="phone" class="form-control" placeholder="TELÉFONO" required="required" data-error="Phone is required.">
                      <div class="help-block with-errors"></div>
                  </div>
              </div>
          </div>

          <div class="row">
              <div class="col-md-3 col-md-push-3">
                  <div class="form-group">
                      <select name="pais" id="form_pais" class="form-control" disabled>
                        <option value="México">México</option>
                      </select>
                  </div>
              </div>

              <div class="col-md-3 col-md-push-3">
                  <div class="form-group">
                    <select name="estado" class="form-control" id="form_estado">
                      <option value="Ciudad de México">Ciudad de México</option>
                      <option value="Aguascalientes">Aguascalientes</option>
                      <option value="Baja California">Baja California</option>
                      <option value="Baja California Sur">Baja California Sur</option>
                      <option value="Campeche">Campeche</option>
                      <option value="Coahuila de Zaragoza">Coahuila de Zaragoza</option>
                      <option value="Colima">Colima</option>
                      <option value="Chiapas">Chiapas</option>
                      <option value="Chihuahua">Chihuahua</option>
                      <option value="Durango">Durango</option>
                      <option value="Guanajuato">Guanajuato</option>
                      <option value="Guerrero">Guerrero</option>
                      <option value="Hidalgo">Hidalgo</option>
                      <option value="Jalisco">Jalisco</option>
                      <option value="México">México</option>
                      <option value="Michoacán de Ocampo">Michoacán de Ocampo</option>
                      <option value="Morelos">Morelos</option>
                      <option value="Nayarit">Nayarit</option>
                      <option value="Nuevo León">Nuevo León</option>
                      <option value="Oaxaca">Oaxaca</option>
                      <option value="Puebla">Puebla</option>
                      <option value="Querétaro">Querétaro</option>
                      <option value="Quintana Roo">Quintana Roo</option>
                      <option value="San Luis Potosí">San Luis Potosí</option>
                      <option value="Sinaloa">Sinaloa</option>
                      <option value="Sonora">Sonora</option>
                      <option value="Tabasco">Tabasco</option>
                      <option value="Tamaulipas">Tamaulipas</option>
                      <option value="Tlaxcala">Tlaxcala</option>
                      <option value="Veracruz de Ignacio de la Llave">Veracruz de Ignacio de la Llave</option>
                      <option value="Yucatán">Yucatán</option>
                      <option value="Zacatecas">Zacatecas</option>
                      </select>
                  </div>
              </div>
          </div>

          <div class="row">
              <div class="col-md-3 col-md-push-3">
                  <div class="form-group">
                      <input id="form_calle"  type="text" name="calle" class="form-control" placeholder="CALLE" required="required" data-error="Street is required.">
                      <div class="help-block with-errors"></div>
                  </div>
              </div>

              <div class="col-md-3 col-md-push-3">
                  <div class="form-group">
                      <input id="form_entre_calle" type="text" name="entre_calle" class="form-control" placeholder="ENTRE CALLE" required="required" data-error="entre calle  is required.">
                      <div class="help-block with-errors"></div>
                  </div>
              </div>

          </div>

          <div class="row">
              <div class="col-md-3 col-md-push-3">
                  <div class="form-group">
                      <input id="form_colonia"  type="text" name="colonia" class="form-control" placeholder="COLONIA" required="required" data-error="Colonia is required.">
                      <div class="help-block with-errors"></div>
                  </div>
              </div>

              <div class="col-md-3 col-md-push-3">
                  <div class="form-group">
                      <input maxlength="5" id="form_cp" type="text" name="cp" class="form-control" placeholder="C.P" required="required" data-error="C P is required.">
                      <div class="help-block with-errors"></div>
                  </div>
              </div>

          </div>

          <div class="row">
              <div class="col-md-2 col-md-push-3">
                  <div class="form-group">
                      <select name="tipo_cafe" id="form_tipo_cafe" class="form-control">
                        <option value="Mezcla de la casa">Mezcla de la casa</option>
                        <option value="Tipo Cubano">Tipo Cubano</option>
                        <option value="Marago">Marago</option>
                        <option value="Descafeinado">Descafeinado</option>
                        <option value="Molido con azúcar">Molido con azúcar</option>
                      </select>
                  </div>
              </div>

              <div class="col-md-2 col-md-push-3">
                  <div class="form-group">
                      <select name="tipo_molido" id="form_tipo_molido" class="form-control">
                        <option value="Molido Fino">Molido Fino</option>
                        <option value="Molido Medio">Molido Medio</option>
                        <option value="Molido Grueso">Molido Grueso</option>
                        <option value="En grano">En grano</option>
                      </select>
                  </div>
              </div>

              <div class="col-md-2 col-md-push-3">
                <div class="form-group">

                  <section class="col-md-4 text-center"><a href="#" class="btn" id="menos_cafe"><i class="fa fa-minus"></i></a></section>

                    <section class="col-md-4">
                      <input id="form_cantidad_cafe" type="text" name="cantidad_cafe" class="form-control" required="required" data-error="Valid email is required." value="0" disabled="true">
                    </section>

                    <section class="col-md-4 text-center"><a href="#" class="btn" id="mas_cafe"><i class="fa fa-plus"></i></a></section>

                </div>
              </div>
          </div>



          <div class="row">
              <div class="col-md-6 col-md-push-3">
                  <div class="form-group">
                      <textarea id="form_message" name="message" class="form-control" placeholder="MENSAJE" rows="4" required="required" data-error="Please,leave us a message."></textarea>
                      <div class="help-block with-errors"></div>
                  </div>
              </div>



              <div class="col-md-12 text-center">
                  <input type="checkbox" id="acepto_aviso" style="" />
                  <label for="acepto_aviso" style="padding-right:10px"><b>Acepto el <a href="assets/AvisoPrivacidadJarocho.pdf" style="color:black">aviso de privacidad</a></p> </label>
                  <input type="submit" id="sendForm" class="btn btn-primary btn-send" value="Enviar">
              </div>




          </div>

      </div>

      <div class="row">
        <section id="success" class="top30"></section>
      </div>

  </form>

</section>


<script>

  var sendButton = document.getElementById('sendForm');
  var checkbox_acept = document.getElementById('acepto_aviso');
  var contactForm = document.getElementById('contact-form-comprar');


  sendButton.addEventListener('click',check_accept_privacy);

  function check_accept_privacy(e){
    e.preventDefault();
    if(!acepto_aviso.checked){
      alert('Tienes que aceptar el aviso de privacidad');
    }
    else{
      submitForm();
    }
  }

  var less_button = document.getElementById("menos_cafe")
  var more_button = document.getElementById("mas_cafe")


  less_button.addEventListener("click",lessCoffee);
  more_button.addEventListener("click",moreCoffee);

  function lessCoffee(e){
    var cafe_container = document.getElementById("form_cantidad_cafe");
    e.preventDefault();

    if(cafe_container.value > 0){
      cafe_container.value = cafe_container.value - 1;
    }

  }

  function moreCoffee(e){
    var cafe_container = document.getElementById("form_cantidad_cafe");
    e.preventDefault();

    cafe_container.value = parseInt(cafe_container.value) + 1;

  }
</script>



<?php require 'footer.php' ?>
