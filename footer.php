<section class="prefooter row">
  <section class="container">
    <section class="col-md-6">
      <!-- SnapWidget -->
  <script src="https://snapwidget.com/js/snapwidget.js"></script>
  <iframe src="https://snapwidget.com/embed/code/188415" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:100%; "></iframe>

    </section>

    <section class="col-md-6 text-center">
      <img src="images/logo-foot.png" alt="" class="height-fix" class="top50">
      <br>
      <img src="images/Tostador.png" class="height-fix2" alt="" class="top15 height-fix">
      <section class="container_social_footer top50" >
            <a href="https://www.facebook.com/cafeljarocho"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></a>
            <a href="https://twitter.com/CAFELJAROCHO"><i class="fa fa-twitter fa-lg" aria-hidden="true"></i></a>
            <a href="https://www.instagram.com/eljarochocafe/"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></a>
      </section>

      <h4 class="top30"><a href="#">#ElAromaDeCoyoacán</a></h4>
      <h4 class="top15"><a href="comprar.php">Haz tu pedido aquí</a></h4>
    </section>
  </section>
</section>

<footer class="text-center">Copyright 2016 <span style="font-size:18px"> © </span> Café El Jarocho. Derechos reservados <a style="padding-left:20px;color: white" href="assets/AvisoPrivacidadJarocho.pdf"> Aviso de privacidad </a> </footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="js/contact_form.js"></script>
<script src="js/contact_form_compra.js"></script>


  <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
  <script src="http://cdn.jsdelivr.net/jquery.scrollto/2.1.2/jquery.scrollTo.min.js"></script>

  <script src="js/vendor/bootstrap.min.js"></script>

  <script src="js/main.js"></script>

  <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
  <script>
      (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
      function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
      e=o.createElement(i);r=o.getElementsByTagName(i)[0];
      e.src='//www.google-analytics.com/analytics.js';
      r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
      ga('create','UA-XXXXX-X','auto');ga('send','pageview');
  </script>
</body>
</html>
