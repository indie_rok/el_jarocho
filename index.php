<?php require 'header_home.php' ?>

     <div id="myCarousel" class="carousel slide " data-ride="carousel">

        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
          <li data-target="#myCarousel" data-slide-to="3"></li>
        </ol>

        <div class="carousel-inner">
          <div class="item active"><img src="images/slide_home1.jpg" style="width:100%" data-src="" alt="First slide"></div>
          <div class="item"> <img src="images/slide_home2.jpg" style="width:100%" data-src="" alt="Second slide"></div>
          <div class="item"> <img src="images/slide_home3.jpg" style="width:100%" data-src="" alt="Third slide"></div>
          <div class="item"> <img src="images/slide_home4.jpg" style="width:100%" data-src="" alt="Third slide"></div>
        </div>
      </div>


      <section class="slide_home" id="nuestra_historia">
        <section class="titulo_chido text-center" >
          <h2>Nuestra historia</h2>
          <h4>tostador, molino y expendio de café</h4>
          <img src="images/titulo_bar.png" class="center-block" alt="">
        </section>


        <section class="contenido_slide container top30">
          <section class="col-md-6">
            <section class="col-xs-4"><img src="images/semillas_cafe.jpg" class="img-responsive"  alt=""></section>
            <section class="col-xs-4"><img src="images/granos_cafe.jpg" class="img-responsive" alt=""></section>
            <section class="col-xs-4"><img src="images/vaso_cafe.jpg"  class="img-responsive" alt=""></section>
          </section>

          <section class="col-md-6" class="text-center">
            <h2 class="text-center top15 uppercase sabor-fix">El sabor y tradición de Coyoacán</h2>
            <h2 class="text-center top15 uppercase sabor-fix" style="color:#bf2424">En un vaso</h2>

            <p class="top30">Nuestra historia se remonta 63 años atrás… Gil Romero y Bertha Paredes (pareja fundadora) trabajaban en una tienda ubicada en la calle de Aguayo, en Coyoacán donde se vendía	n semillas y frutas traídas de Veracruz. Posteriormente Bertha Paredes queda como dueña de ese negocio que mas tarde se cambiaría de lugar a las calles de Cuauhtémoc y Allende en también en Coyoacán México, dando lugar a Café El Jarocho.</p>
            <section class="text-center"><a href="historia.php" class="btn btn-primary">Leer más</a></section>
          </section>

        </section>
      </section>

      <section class="slide_home bg_gray"  id="nuestros_productos">
        <section class="titulo_chido text-center">
          <h2>Nuestros Productos</h2>
          <h4>aroma con sabor a tradicion</h4>
          <img src="images/titulo_bar.png" class="center-block" alt="">

        </section>

        <section class="contenido_slide row top30">
          <section class="col-md-4 nopadding">
            <a href="menu.php"><img src="images/menu_cafe.jpg" alt="" class="img-responsive"></a>
          </section>
          <section class="col-md-4 nopadding">
            <a href="menu.php"><img src="images/menu_granocafe.jpg" alt="" class="img-responsive"></a>
          </section>
          <section class="col-md-4 nopadding">
            <a href="menu.php"><img src="images/menu_pan.jpg" alt="" class="img-responsive"></a>
          </section>
        </section>
      </section>

      <section class="slide_home"  id="nuestras_sucursales">
        <section class="titulo_chido text-center">
          <h2>Sucursales</h2>
          <h4>estamos cerca de tí</h4>
          <img src="images/titulo_bar.png" class="center-block" alt="">

        </section>

        <section class="contenido_slide top30">
          <div id="carrousel2" class="carousel slide" data-ride="carousel">

            <ol class="carousel-indicators">
              <li data-target="#carrousel2" data-slide-to="0" class="active"></li>
              <li data-target="#carrousel2" data-slide-to="1"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
              <div class="item active">
                <a href="sucursales.php"><img src="images/resturant-img.jpg"  style="width:100%" alt="Chania"></a>
              </div>

              <div class="item">
                <a href="sucursales.php"><img src="images/resturant-img2.jpg"  style="width:100%" alt="Chania"></a>
              </div>
            </div>
          </div>
        </section>
      </section>


      <section class="slide_home"  id="nuestro_contacto">

        <section class="titulo_chido text-center">
          <h4>sigamos en</h4>
          <h2>CONTACTO</h2>
          <img src="images/titulo_bar.png" class="center-block" alt="">

        </section>

        <form id="contact-form" class="top30 contact-form " method="post" action="contacto.php" role="form"  data-toggle="validator" >
        <div class="messages"></div>

            <div class="controls">
                <div class="row">
                    <div class="col-md-6 col-md-push-3">
                        <div class="form-group">
                            <input id="form_name" type="text" name="name" class="form-control" placeholder="NOMBRE" required="required" data-error="Firstname is required.">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-md-push-3">
                        <div class="form-group">
                            <input id="form_email" type="email" name="email" class="form-control" placeholder="E-MAIL" required="required" data-error="Valid email is required.">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-md-push-3">
                        <div class="form-group">
                            <textarea id="form_message" name="message" class="form-control" placeholder="MENSAJE" rows="4" required="required" data-error="Please,leave us a message."></textarea>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                        <input type="submit" class="btn btn-primary btn-send" value="Enviar">
                    </div>
                </div>

            </div>

            <section id="success" class="top30"></section>

        </form>
      </section>

<?php require 'footer.php' ?>
